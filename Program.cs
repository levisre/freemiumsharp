﻿using System.Net.Mime;
using AsmResolver.DotNet;
using AsmResolver.DotNet.Code.Cil;
using AsmResolver.PE.DotNet.Cil;
using AsmResolver.DotNet.Signatures;
using Microsoft.Win32;


namespace FreemiumSharp
{

    internal class Program
    {
        private static ModuleDefinition _mod = new("Dummy");

        private static string? FindJbTarget()
        {
            const string parentKey = @"SOFTWARE\JetBrains\ReSharperPlatformVs17";
            Console.WriteLine("Checking registry...", ConsoleColor.Cyan);
            var regDir = Registry.CurrentUser.OpenSubKey(parentKey)?.GetSubKeyNames();
            if (regDir == null)
            {
                Console.WriteLine("JB Not found!", ConsoleColor.Red);
                Environment.Exit(0);
            }
            var regSubKey = parentKey + "\\" + regDir.First();
            return (string?)Registry.CurrentUser.OpenSubKey(regSubKey)?.GetValue("InstallDir");
        }

        private static TypeDefinition? FindType(ModuleDefinition module, string path)
        {
            var types = module.GetAllTypes();
            return types.FirstOrDefault(t => t.FullName == path);
        }

        private static List<CilInstruction> BoolPatch(bool val)
        {
            return new List<CilInstruction>(2)
            {
                new (val ? CilOpCodes.Ldc_I4_1 : CilOpCodes.Ldc_I4_0),
                new (CilOpCodes.Ret)
            };
        }

        private static List<CilInstruction> NullPatch()
        {
            return new List<CilInstruction>(2) {
                new (CilOpCodes.Ldnull),
                new (CilOpCodes.Ret) };
        }

        private static List<CilInstruction> ClearBodyPatch()
        {
            return new List<CilInstruction>(1)
            {
                new (CilOpCodes.Ret)
            };
        }

        private static List<CilInstruction> StringPatch(string val)
        {
            return new List<CilInstruction>(2)
            {
                new(CilOpCodes.Ldstr, val), new (CilOpCodes.Ret)
            };
        }

        private static List<CilInstruction> IntPatch(int val)
        {
            return new List<CilInstruction>(2)
            {
                new (CilOpCodes.Ldc_I4, val),
                new (CilOpCodes.Ret)
            };
        }

        private static List<CilInstruction> DateTimePatch(int y, int m, int d)
        {
            // Ref: https://github.com/Washi1337/AsmResolver/issues/303
            var factory = _mod.CorLibTypeFactory;
            var dateTimeCtor = factory.CorLibScope
                .CreateTypeReference("System", "DateTime")
                .CreateMemberReference(".ctor",
                    MethodSignature.CreateInstance(factory.Void, factory.Int32, factory.Int32, factory.Int32))
                .ImportWith(_mod.DefaultImporter);
            return new List<CilInstruction>(5)
            {
                new (CilOpCodes.Ldc_I4, y),
                new (CilOpCodes.Ldc_I4, m),
                new (CilOpCodes.Ldc_I4, d),
                new (CilOpCodes.Newobj, dateTimeCtor),
                new (CilOpCodes.Ret)
            };
        }

        private static bool DoPatch(string userName, string lic)
        {
            if (userName.Length == 0) return false;
            if (lic.Length == 0) return false;

            // Find all target classes
            var licCheckerClass = FindType(_mod,"JetBrains.Application.License.LicenseChecker");
            if (licCheckerClass == null) return false;
            var licDataClass = FindType(_mod, "JetBrains.Application.License.LicenseData");
            if (licDataClass == null) return false;
            var resultExClass = FindType(_mod, "JetBrains.Application.License2.ResultEx");
            if (resultExClass == null) return false;
            var licenseViewClass = FindType(_mod, "JetBrains.Application.License2.UserLicenses.UserLicenseViewSubmodel");
            if (licenseViewClass == null) return false;
            var licStatusClass = FindType(_mod, "JetBrains.Application.License2.UserLicenses.UserLicenseStatus");
            if (licStatusClass == null) return false;
            var resultDescriptionClass = FindType(_mod, "JetBrains.Application.License2.ResultWithDescription");
            if (resultDescriptionClass == null) return false;
            var licEntityClass = FindType(_mod, "JetBrains.Application.License2.LicensedEntityEx");
            if (licEntityClass == null) return false;

            // Perform Patching for each method
            var isChecksumOkMethod = new TargetMethod(licCheckerClass, "get_IsChecksumOK");
            isChecksumOkMethod.ApplyPatch(BoolPatch(true));
            var hasLicenseMethod = new TargetMethod(licCheckerClass, "get_HasLicense");
            hasLicenseMethod.ApplyPatch(BoolPatch(true));
            var licTypeMethod = new TargetMethod(licCheckerClass, "get_Type");
            licTypeMethod.ApplyPatch(IntPatch(0));
            var licStatus = int.TryParse(lic.Substring(0, lic.IndexOf('-')), out var num);
            if (!licStatus) num = 0xDEAD;
            var customerIdMethod = new TargetMethod(licCheckerClass, "get_CustomerId");
            customerIdMethod.ApplyPatch(IntPatch(num));

            var expirationDateMethod = new TargetMethod(licDataClass, "get_ExpirationDate");
            expirationDateMethod.ApplyPatch(DateTimePatch(2038, 12, 31));
            var subEndDateMethod = new TargetMethod(licDataClass, "get_SubscriptionEndDate");
            subEndDateMethod.ApplyPatch(DateTimePatch(2038, 12, 31));
            var licGenDateMethod = new TargetMethod(licDataClass, "get_GenerationDate");
            licGenDateMethod.ApplyPatch(DateTimePatch(2021, 1, 1));
            var licEndlessMethod = new TargetMethod(licDataClass, "get_IsEndless");
            licEndlessMethod.ApplyPatch(BoolPatch(true));
            var licContainSubMethod = new TargetMethod(licDataClass, "get_ContainsSubscription");
            licContainSubMethod.ApplyPatch(BoolPatch(true));
            var licKeyMethod = new TargetMethod(licDataClass, "get_LicenseKey");
            licKeyMethod.ApplyPatch(StringPatch(lic));
            var licUserNameMethod = new TargetMethod(licDataClass, "get_UserName");
            licUserNameMethod.ApplyPatch(StringPatch(userName));
            var licDataCustomerIdMethod = new TargetMethod(licDataClass, "get_CustomerId");
            licDataCustomerIdMethod.ApplyPatch(IntPatch(num));
            var licDataLicTypeMethod = new TargetMethod(licDataClass, "get_LicenseType");
            licDataLicTypeMethod.ApplyPatch(IntPatch(0));

            var resultDescMethod = new TargetMethod(resultDescriptionClass, "get_Result");
            resultDescMethod.ApplyPatch(IntPatch(0));

            var requireLicMethod = new TargetMethod(licEntityClass, "RequiresLicense");
            requireLicMethod.ApplyPatch(BoolPatch(false));

            var oldLicDataMethod = new TargetMethod(licenseViewClass, "TryCreateInfoForOldLicenseData");
            oldLicDataMethod.ApplyPatch(NullPatch());
            var checkLicMethod = new TargetMethod(licenseViewClass, "CheckLicense");
            checkLicMethod.ApplyPatch(ClearBodyPatch());

            var successMethod = new TargetMethod(resultExClass, "IsSuccessful");
            successMethod.ApplyPatch(BoolPatch(true));
            var warningMethod = new TargetMethod(resultExClass, "ContainsWarnings");
            warningMethod.ApplyPatch(BoolPatch(false));
            var minShutdownMethod = new TargetMethod(resultExClass, "Is30MinToShutdown");
            minShutdownMethod.ApplyPatch(BoolPatch(false));
            var failedMethod = new TargetMethod(resultExClass, "IsFailed");
            failedMethod.ApplyPatch(BoolPatch(false));

            var severityMethod = new TargetMethod(licStatusClass, "get_Severity");
            severityMethod.ApplyPatch(IntPatch(0));
            return true;
        }
        
        static void Main(string[] args)
        {
            if (args.Length == 0) Console.WriteLine("NOTE: Can pass username to arg", ConsoleColor.Green);
            string? targetDir = FindJbTarget();
            if (targetDir == null) { 
                Console.WriteLine("JB Not found!", ConsoleColor.Red);
                Console.ReadKey();
                Environment.Exit(0);
            }
            string targetFile = targetDir + "\\JetBrains.Platform.Shell.dll";
            string backupName = targetFile + ".bak";
            if (!File.Exists(backupName))
            {
                Console.WriteLine("Creating Backup...", ConsoleColor.Green);
                File.Copy(targetFile, backupName);
            }

            try
            {
                _mod = ModuleDefinition.FromFile(targetFile);
                var result = DoPatch(args.Length == 1 ? args[0]:"Levis", "1234-1234-1234-1234");
                if (!result)
                {
                    Console.WriteLine("Something not work!", ConsoleColor.Yellow);
                    Environment.Exit(0);
                }
                File.SetAttributes(targetFile, FileAttributes.Normal);
                _mod.Write(targetFile);
                Console.WriteLine("Done!", ConsoleColor.Green);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}", ConsoleColor.Red);
            }
        }

    }

    public class TargetMethod
    {
        // Reserved
        //private TypeDefinition parent;
        //private string name;
        private static MethodDefinition? _methodObj;
        public TargetMethod(TypeDefinition type, string name)
        {
            //parent = type ?? throw new ArgumentNullException("type");
            //this.name = name ?? throw new ArgumentNullException("name");
            _methodObj = FindMethod(type, name);
        }
        private static MethodDefinition? FindMethod(TypeDefinition type, string name)
        {
            return type.Methods.FirstOrDefault(method => method.Name == name);
        }

        public bool ApplyPatch(List<CilInstruction> instructionsList)
        {
            if (_methodObj == null) return false;
            if (instructionsList.Count == 0) return false;
            try
            {
                CilMethodBody body = new (_methodObj);
                _methodObj.CilMethodBody?.Instructions.Clear();
                foreach (var instr in instructionsList)
                {
                    body.Instructions.Add(instr);
                }

                _methodObj.CilMethodBody = body;
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
